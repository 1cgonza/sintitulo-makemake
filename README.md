# Ruta de desarrollo

[[_TOC_]]

## General

- Definir nombre del proyecto (Catalina)
- Propongo usar Github para que cuando se haga público tenga más visibilidad.

## Librería

### Configuración

- Desarrollo en typescript para mantener consistencia y claridad en el equipo que desarrolla.
- Compilación con Rollup, exporta cjs y esm para importar librearía completa por link o por módulos.
- Incluir auto documentación que puede ser con `tsd-jsdoc`
- Usar Prettier con `.editorconfig` para mantener estilos de código consistentes en todo el equipo.

### Módulos

En términos generales el objetivo es darle versatilidad a lo que se viene usando para desarrollar los libros. Que se puedan usar módulos de esta librería en nuevos proyectos con agilidad, pero que también permita crear cosas que a lo mejor no se han pensado para los libros al facilitar ciertas funciones. Se busca estabilizar cosas que se viene realizando de diferentes formas: animación, escalar a pantallas, trabajo con diferentes tipos de assets, etc.

#### Principal

Con el que se inicia cualquier proyecto, debe permitir un `id` o `HTML Element` para contener un libro. Es agnóstico a la herramienta, sirve para HTML o React, Vue, etc.

#### Matemática

Este es el módulo madre para lo que sucede en pantalla, de aquí se definen muchas estrategias de como se optimiza la velocidad de escalar, animar y diagramar los elementos en pantalla.

La idea inicial para poner a prueba es usar matrices. Esto implica una abstracción de las coordenadas en pantalla a valores normalizados (-1 a 1). Para trabajar con proporciones y no pixeles, también permitiría posiblemente usar WebGL con el mismo sistema de coordenadas si se llega a eso. En el caso de renderizar usando CSS, usar matrices obliga a que dispositivo delegue automáticamente el proceso al GPU y no al CPU que es más rápido.

La segunda opción es usar porcentajes y píxeles para hacer la matemática de animación y composición.

La idea en ambos casos es quitarle las dificultades matemáticas al usuario con funciones nombradas de manera clara y tener suficientes opciones para libremente hacer operaciones repetitivas llamando los métodos aquí dispuestos.

#### Página

Depende del principal y agrega submódulos al principal.

#### Elementos

Permite agregar al principal o página un elemento. Se divide en submódulos que controlan cada tipo de elemento:

- imagen: Estáticas y de composición.
- video: Recibe url de archivos, comprueba que existan los necesarios, expone eventos para controlar su reproducción, expone métodos para composición.
- sonido: Expone eventos.
- sprite: recibe coordenadas en módulo o JSON, expone métodos para composición y animación.
- texto: Expone métodos de composición y animación.

#### Motor de animación

Por definir si privilegiamos canvas o si se hace también un motor para animación CSS.

#### Gestos y Control

Si el motor de animación es CSS podemos usar los que incluye JS sobre elementos HTML, en canvas toca desarrollarlos y exponerlos como eventos. Estos son cosas como clic, touch, coordenadas del mouse, posición del scroll, etc.

#### Menú

Casi todos los libros tienen menú y su diagramación debe depender de un desarrollo propio en CSS así que debe ser agnóstico al diseño. Creo que puede permitir formas de diagramar y usar gestos para su control de manera básica y luego delegar el resto de su funcionamiento al diseño del usuario.

## Software de Assetts

Desarrollarlo en Electron para ser exportado como aplicación de Windows, Mac o Linux. Recibe principalmente archivos de Photoshop para convertirlos en sprites y código de coordenadas en JS o JSON.

### Interfaz Gráfica

Asignar una persona del equipo para liderar esta parte. Conocimiento en SCSS y HTML principalmente.

### Secciones

#### Importar

Acepta archivos Photoshop, entiende su estructura y dispone sus partes para ser organizadas en la interfaz gráfica.

#### Plantillas

Sección donde se puedan traer imagenes individualmente para construir un sprite.

#### Optimizador

Hace un primer intento por organizar las capas de Photoshop en un spritesheet y debe permitir al usuario agrupar y corregir.

#### Visualizador

Debe mostrarnos la organización del sprite, poder ver si las coordenadas que se van a exportar tienen sentido, y en caso de ser animación, poderlas reproducir en ciclo.

#### Exportar

Nos muestra el código con coordenadas, debe permitir exportar éste en código JS o archivo JSON. 

Muestra el sprite final y permite definir la calidad, formato de la imagen y nombre que se le da al archivo antes de descargarlo.

### Módulos internos

#### Control de secciones

Cada sección tiene su propio módulo para controlar las funciones descritas anteriormente.

#### Imagenes

Idealmente se usa Imagemagick pero es muy grande para incluirlo completo, hay que buscar en NPM una opción liviana de control de imagen para que valga la pena.

#### Base de datos

Definir si usamos `store` de Vue o si usamos una base de datos en documentos tipo `LokiJS` o `electron-db`. Esto para poder almacenar configuraciones sobre un archivo y permitirle al usuario volver a estos y modificar la confirguración o modos de exportar sin tener que volver a hacer todo el proceso de nuevo.

Podriamos guardar aquí algunas plantillas creadas por los usuarios para que se apliquen directamente en archivos nuevos usando como base la configuración de un archivo anterior.

## Vocabulario y condiciones para desarrollo

### Librearía JS

El software en JS con el que se crean los libros. Todo en TypeScript y sus variables, métodos y nombres se escriben en idioma español (pero sin tildes AGHH) para automatizar la documentación.

### Variables

Usamos sólo ES5 para evitar ambigüedades en las variables, nunca usar `var` siempre `let` o `const` en el desarrollo. Si algo no va a cambiar, usamos `const` y sólo en caso de variables dinámicas usamos `let`. Siempre privilegiar `const`. Nunca usar variables globales como regla general y escribirlas en español.

### Método

Es cada función disponible, escritas únicamente en camelCase, nunca con guiones (`_` o `-`), sus nombres deben ser explícitos y claros en idioma español. Privilegiar siempre claridad en el nombre sobre extensión, es decir, nunca nombrar variables o métodos sin que se puedan leer en español y se entienda perfectamente. Por ejemplo:

**INCORRECTO**

```ts
function m(a: number, b: number) {
  var t = //...
}
```

**CORRECTO**

```ts
function matriz(a: number, b: number) {
  const transformacion = //...
}
```

La primera es corta pero ambigua, la segunda es larga pero clara. La minificación al final se encarga de convertir el segundo en el primero para reducir bytes. Las excepciones a esto son abreviaciones ya integradas al lenguaje cotidiano como: `x`, `y`, `z` para coordenadas, o `i` para indices, `j` para segundo indice encadenado, `a`, `b`, ... para parámetros de orden numérico como el ejemplo anterior.

Los métodos públicos con los que dispone el usuario se escriben simplemente en _camelCase_ y los privados con guion al piso, Ej: `_metodoPrivado`. La primera letra en cualquier caso es siempre minúscula.

### Clase

Todos los módulos de la librearía son clases de ES6 a excepción de funciones de ayuda. Se nombran en español y siempre con la primera letra en mayúscula para diferenciarlos de métodos. Se pueden hacer subclases usando `extend` si cumple diferentes funciones.

### Documentación

Todo debe estar ampliamente documentado usando las convenciones [JSDOC](https://jsdoc.app/). Las descripciones en español aquí sí con tildes y redacción clara. Estas descripciones dentro del código luego se exponen en la documentación de la librearía en la página. Esta documentación se debe generar programáticamente usando rollup y algún módulo de NPM que convierta documentación de TypeScript - JSDOC en una página estática o markdown. Es decir, no vamos a documentar aparte sino dentro del mismo cuerpo del código.
